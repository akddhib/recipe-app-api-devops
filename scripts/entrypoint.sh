#!/bin/sh

set -e

# uwsgi is good in serving python files but not optimized to serve static files
# so it is recommended to server them through a proxy
# use "collectstatic" to collect all static files required for the project and store them in a single directory
# --noinput suppress any questions asked to the user when it runs the command -- yes to everything
python manage.py collectstatic --noinput

# it is recommended to run our db migration whenever you start the application. 
# Django wait for database, polls the database every second and then execute the following commands
python manage.py wait_for_db


# Django has a great features to manage database migration, you just create a migration file 
# and Django will make the necessary changes on the database (Adding table, field, remove a table..)

python manage.py migrate

# run the uwsgi service as a master service in the terminal, good for closing down 
# run the app.wsgi from our "app" directory
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
