terraform {
  backend "s3" {
    bucket         = "recipe-app-backend-state"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-api-dynamo-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.36.0"
}

# Interpolation syntax : adjust local prefix by workspace
locals {
  prefix = "${var.prefix}-${terraform.workspace}"

  # add common_tags to be applied to all resources in this project
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"

  }
}

# retrive the current region
data "aws_region" "current" {}