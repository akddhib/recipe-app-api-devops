resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-files"
  # files on this bucket are publicly available
  acl = "public-read"
  # destroy our bucket with our terraform
  force_destroy = true
}
