data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

# add record for our actual load balancer
resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
  # link it to CNAME which points to another DNS name of the load balancer
  type = "CNAME"
  # 5 min to propagate dns service
  ttl = "300"

  records = [aws_lb.api.dns_name]
}

# Create an SSL/TLS certificate

resource "aws_acm_certificate" "cert" {
  domain_name       = aws_route53_record.app.fqdn
  validation_method = "DNS"

  tags = local.common_tags

  # Recommended to prevent any erros when destroying the env
  lifecycle {
    create_before_destroy = true
  }
}

# Create a record for the purpose of main validation
resource "aws_route53_record" "cert_validation" {
  name    = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_value
  ]
  # take up to 1 min in order for validation to complete
  ttl = "60"
}

# validate the certificate
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}
