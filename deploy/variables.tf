# prefix used for resources naming
variable "prefix" {
  default = "rad"
}

variable "project" {
  default = "recipe-add-devops"
}

variable "contact" {
  default = "dhiabi.akram@gmail.com"
}

variable "db_username" {
  description = " Username for the Postgres instance"
}

variable "db_password" {
  description = " Password for the Postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "856168807003.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "856168807003.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "akddevops.link"
}

# specify the subdomains that we are goint to use to split up domain name into subdomains per environment
variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
