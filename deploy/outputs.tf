output "db_host" {
  # internal network address of our db server
  # help us debug our database through our bastion host if needed
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_route53_record.app.fqdn
}
